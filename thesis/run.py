"""
the run module will go through all of the steps to build and 
test a neural network
"""

# imports
import model as md
import data
import plotting
from datetime import datetime as dt

"""
generate_test_model goes through all of the steps to build and test a 
neural network of choice and tests the model

Inputs:
    url: string - location of CSV data file
    features: list - a list of strings with the names of the columns
        to be used in the neural network
    timesteps: int - the number of timesteps to be used
    batch_size: int - number of batches
    epochs: int - epochs to use
    arch: string - NN architecture to use (rnn, gru, lstm, tcn)
    hidden (opt): int list -  list of integers to be used to determine 
    index_col (opt): int - column to be used as the
        index column in the dataframe
    lr (opt): float - learning rate
    val_split (opt): float - a float value < 1 to be used to split the data
            into train/test series.  E.g. 0.8 would give 80% train data and
            20% test data.
           
            As a part of a different module (model.fit_model()), training 
            data will be split further into training and validation data.  
            By default, the data will be split 80/20.
    verbosity (opt): int - logging verbosity
    shuffle (opt): bool - Shuffle data 
    activation (opt): string - the activation function to use
    loss (opt): string - loss function to use
    x_label (opt): string - title of x axis on data plot, 
    y_label (opt): string - title of y axis  data plot, 
    title (opt): string- plot title on data plot      
    save (opt): bool - save model
    file_name (opt) - name to save model as
Returns:
    mse: float - mean squared error
    rmse: float - root mean squared error
    r2_value: float - r^2 value
"""
def generate_test_model(
        url, features, timesteps, batch_size, epochs, arch, 
        hidden=[], index_col=0, lr=0.001, val_split=0.2,
        verbosity=0, shuffle=False, activation='relu', 
        loss='mean_squared_error',x_label='', 
        y_label='', title='', save=True, file_name=''):

    # pull in data from csv as pandas dataframe
    df = data.import_data(url, index_col)
    
    # generate train and test series
    train_data, test_data = data.generate_series(df, features)
    
    # normalize train and test series
    train, test = data.normalize_data(train_data, test_data)
    
    # build and train model
    model, train_loss,val_loss = md.fit_model(train, timesteps, 
                                    epochs, batch_size, arch,
                                    hidden, lr=lr, val_split=val_split,
                                    verbosity=verbosity, shuffle=shuffle,
                                    activation=activation, loss=loss)
    # evaluate model
    true, predicted, mse, rmse, r2_value = md.test_model(model, 
                                                         test, 
                                                         timesteps)
    
    # plot data
    plotting.plot_data(true, predicted, x_label=x_label, 
                    y_label=y_label, title=title)
    
    # show error
    print('MSE = {}'.format(mse))
    print('RMSE = {}'.format(rmse))
    print('R-Squared Score = {}'.format(r2_value))
    
    # plot loss
    plotting.plot_error(train_loss, val_loss)

    # save model
    if save:
        if file_name == '':
            file_name = './models/'+arch+dt.now().strftime("%Y-%m-%d_%H:%M")
            md.change_model('save', file_name=file_name, model=model)
        else: md.change_model('save', file_name=file_name, model=model)
        
    return mse, rmse, r2_value
        
"""
test_model takes data and runs through an existing model

Inputs:
    file_name: str -path to model directory
    url: string - location of CSV data file
    features: list - a list of strings with the names of the columns
        to be used in the neural network
    timesteps: int - the number of timesteps to be used    
    index_col (opt): int - column to be used as the
        index column in the dataframe    
    x_label (opt): string - title of x axis on data plot, 
    y_label (opt): string - title of y axis  data plot, 
    title (opt): string- plot title on data plot     
Returns: n/a
"""
        
def test_model(file_name, url, features, timesteps, index_col=0,
               x_label='', y_label='', title=''):
    # load model
    model = md.change_model('load', file_name=file_name)
    
    # pull in data from csv as pandas dataframe
    df = data.import_data(url, index_col)
    
    # generate train and test series
    test_data, empty = data.generate_series(df, features, 
                                            split_value=1)
    
    # normalize train and test series
    test, empty = data.normalize_data(test_data, test_data)
        
    
    # evaluate model
    true, predicted, mse, rmse, r2_value = md.test_model(model, 
                                                         test, 
                                                         timesteps)
    
    # plot data
    plotting.plot_data(true, predicted, x_label=x_label, 
                    y_label=y_label, title=title)     
    
    # show error
    print('MSE = {}'.format(mse))
    print('RMSE = {}'.format(rmse))
    print('R-Squared Score = {}'.format(r2_value))
    