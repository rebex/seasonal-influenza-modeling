"""
plotting module plots returned data from neural networks
"""

# imports
from matplotlib import pyplot as plt

"""
plot_data rplots actual data and predicted data
vs time.  

Inputs: 
    y_test: list - containing test data, 
    y_hat: list - containing predicted data, 
    x_label (opt): string - title of x axis, 
    y_label (opt): string - title of y axis, 
    title (opt): string- plot title

Returns: n/a 
"""
  
def plot_data(y_test, y_hat, x_label='', y_label='', title=''):
    plt.plot(y_test,c = 'k')
    plt.plot(y_hat,c = 'm')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.legend(['Actual', 'Predicted'],loc = 'upper left')
    plt.show() 


"""
plot_error returns a plot chosing training and validation error
vs epochs

Inputs:
    train_loss: numpy array - showing training loss determined by loss 
    function of model,
    val_loss: numpy array - showing validation loss,
    y_label(opt): string - title of y axis
    x_label(opt): string - title of x axis
    title(opt): string - title of plot 
    
Returns:
    n/a
"""

def plot_error(train_loss, val_loss):
    plt.plot(train_loss,c = 'k')
    plt.plot(val_loss,c = 'm')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.title('Train and Validation Loss')
    plt.legend(['train','val'],loc = 'upper right')
    plt.show()
