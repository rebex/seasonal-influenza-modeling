"""
layers module returns layers generated for neural networks
TODO: consider using an overloaded "build_network" method
"""

# imports
from keras.models import Sequential
from tensorflow.keras.layers import Dense as dense
from keras.layers.core import Dense
from keras.layers.recurrent import SimpleRNN, LSTM, GRU
from keras import optimizers
from tensorflow.keras import Input, Model
from tcn import TCN, tcn_full_summary

"""
add_rnn builds a multivariate RNN model using Keras' SimpleRnn 

Inputs:
    X_train: numpy array - containing the data to be used to 
            train the network
    y_train: numpy array - containing the expected outcomes
    hidden: int list -  list of integers to be used to determine 
            number of units for each hidden layer within the network
    lr (opt): float - learning rate
    activation (opt): string - the activation function to use
    loss (opt): string - loss function to use 

Returns:
    model - Sequential - a Sequential object from keras.models
"""

def add_rnn(X_train, y_train, hidden, lr=0.001, 
            activation='relu', loss='mean_squared_error'):    
    model = Sequential()
    model.add(SimpleRNN(X_train.shape[2], input_shape = 
                        (X_train.shape[1], X_train.shape[2]),
                        return_sequences=True,
                        activation=activation))
    for i in range(len(hidden)-1):        
        model.add(SimpleRNN(hidden[i], activation=activation,
                            return_sequences=True))
    model.add(SimpleRNN(hidden[-1], activation=activation))
    model.add(Dense(1))
    model.compile(optimizer=optimizers.Adam(lr=lr), 
                  loss=loss)
    
    # print(model.summary())
    return model 

"""
add_lstm builds a multivariate LSTM model using Keras' LSTM 

Inputs:
    X_train: numpy array - containing the data to be used to 
            train the network
    y_train: numpy array - containing the expected outcomes
    hidden: int list -  list of integers to be used to determine 
            number of units for each hidden layer within the network
    lr (opt): float - learning rate
    activation (opt): string - the activation function to use
    loss (opt): string - loss function to use 

Returns:
    model - Sequential - a Sequential object from keras.models
"""

def add_lstm(X_train, y_train, hidden, lr=0.001, 
            activation='relu', loss='mean_squared_error'): 
    model = Sequential()
    model.add(LSTM(X_train.shape[2],input_shape = 
                   (X_train.shape[1],X_train.shape[2]),
                   return_sequences=True,
                   activation=activation))
    for i in range(len(hidden)-1):        
        model.add(LSTM(hidden[i], activation=activation,
                       return_sequences=True))
    model.add(LSTM(hidden[-1], activation='relu'))
    model.add(Dense(1))
    model.compile(optimizer=optimizers.Adam(lr=lr), 
                  loss=loss)
    
    # print(model.summary())
    return model

"""
add_gru builds a multivariate GRU model using Keras' GRU 

Inputs:
    X_train: numpy array - containing the data to be used to 
            train the network
    y_train: numpy array - containing the expected outcomes
    hidden: int list -  list of integers to be used to determine 
            number of units for each hidden layer within the network
    lr (opt): float - learning rate
    activation (opt): string - the activation function to use
    loss (opt): string - loss function to use 

Returns:
    model - Sequential - a Sequential object from keras.models
"""
         
def add_gru(X_train, y_train, hidden, lr=0.001, 
            activation='relu', loss='mean_squared_error'):
    model = Sequential()
    model.add(GRU(X_train.shape[2],input_shape= 
                  (X_train.shape[1],X_train.shape[2]),
                  return_sequences=True,
                  activation=activation))
    for i in range(len(hidden)-1):        
        model.add(GRU(hidden[i],activation=activation,
                      return_sequences=True))
    model.add(GRU(hidden[-1],
                  activation=activation))
    model.add(Dense(1))
    model.compile(optimizer=optimizers.Adam(lr=lr), loss=loss)
 
    # print(model.summary)
    return model

"""
add_tcn builds a multivariate TCN model using github user philipperemy's
TCN (https://github.com/philipperemy/keras-tcn based on 
     https://arxiv.org/pdf/1803.01271.pdf )

Inputs:
    input_shape: int - used to determine input shape for TCN
    batch_size = int - batch size
    timesteps = int - timesteps
    lr (opt): float - learning rate
    loss (opt): string - loss function to use 

Returns:
    model - Model - a Model object
"""
    
def add_tcn(input_shape, batch_size, timesteps, 
            lr=0.001, loss='mean_squared_error'): 
    inputs = Input(batch_shape=(batch_size, timesteps, input_shape))
    print(inputs)
    outputs = TCN(return_sequences=False)(inputs)
    outputs = dense(1)(outputs)
    
    model = Model(inputs=[inputs], outputs=[outputs])
    model.compile(optimizer='adam', loss=loss)
    
    tcn_full_summary(model, expand_residual_blocks=False)
    
    # print(model.summary)
    return model
    
