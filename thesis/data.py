"""
data module plots modifies data to be used in neural networks
"""

# imports
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

"""
import_data imports data from a CSV using a pandas dataframe

Inputs:
    url: string - location of CSV data file
    index_col (opt): int - column to be used as the
    index column in the dataframe
    
Returns:
    a dataframe containing the data from the CSV
"""

def import_data(url, index_col=0):
    df = pd.read_csv(url, index_col=index_col)
    print('Data successfully imported from %s.' 
          % url)
    return df

"""
generate_series creates two series from input data by splitting 
the data into train and test categories using only the selected 
column names from the dataframe

Inputs:
    df: pd dataframe - containing at least two columns of data
    features: list - a list of strings with the names of the columns
            to be used in the neural network
    split_value (opt): float - a float value < 1 to be used to split the data
            into train/test series.  E.g. 0.8 would give 80% train data and
            20% test data.
            
            As a part of a different module (model.fit_model()), training 
            data will be split further into training and validation data.  
            By default, the data will be split 80/20.
            
Returns:
    train_data: pd dataframe
    test_data: pd dataframe
"""

def generate_series(df, features, split_value=0.8):
    # extract the series   
    series = df[features]
    
    # split series into train and test data
    split = int(series.shape[0]*split_value)
    
    train_data = series[:split]
    test_data = series[split+1:]
    
    return train_data, test_data

"""
normalize_data uses the MinMaxScaler from the sklearn python library
to normalize the data.  The data is fit to the training data, and transforms
the training and test data.

Inputs:
    train_data: pd dataframe - returned from generate_series()
    test_data: pd dataframe - returned from generate_series()
Returns:
    train: numpy array - normalized training data
    test: numpy array - normalized test data
"""

def normalize_data(train_data, test_data):  
    sc = MinMaxScaler()
    train = sc.fit_transform(train_data)
    test = sc.transform(test_data)

    return train, test


