"""
batch module has runs batch files
"""

# imports
import csv
import os
import run

"""
get_files navigates to the directory containing the data to be ran in through
the batch process.  The directory must be a subfolder of the current directory)

Inputs:
    data_dir: str - path to directory containing data

Returns: 
    file_list: list - list of strings.  Strings are the names of the files
            in the directory.
"""

def get_files(data_dir):
    
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    directory = os.path.join(ROOT_DIR, data_dir)
    file_list = []
    
    for filename in os.listdir(directory):
        if filename.endswith(".csv"):
            file_list.append(filename)
        else:
            continue
    
    return file_list

"""
write_to_csv outputs results to an existing csv file

Inputs:
    file_name: str - the name of the .csv file to store the results
    result: list - a list containing the results to be written
Returns: n/a
"""

def write_to_csv(file_name, results):
    
    with open(file_name, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', 
                            quoting=csv.QUOTE_MINIMAL)
        writer.writerow(results)

"""
batch_runner runs a batch of files through a given set of architectures
('rnn', 'lstm', 'gru', 'tcn') using a given set of features

Inputs: 
    data_dir: str - path to directory containing data (csv) to be tested
    feature_list: list - list containing lists of features within the csv
            files in the data_dir 
            (e.g. [['feature1', 'feature2'], ['feature1', 'feature3']])
    arch_list: list - list of architectures to test during the batch process
    timesteps: int - the number of timesteps to be used
    epochs: int - epochs to use
    batch_size: int - number of batches
    arch: string - NN architecture to use (rnn, gru, lstm, tcn)
    hidden (opt): int list -  list of integers to be used to determine 
    number of units for each hidden layer within the network
    lr (opt): float - learning rate
    val_split (opt): float - percentage of data to be used as validation
                data (remainder is training)
    verbosity (opt): int - logging verbosity (if you would like to watch each
                epoch of each model, select 1)
    shuffle (opt): bool - Shuffle data 
    activation (opt): string - the activation function to use
    loss (opt): string - loss function to use     

Returns: n/a    
"""

# example inputs for batch_runner()
# feature_list = [
#         ['total_ili', 'total_patients'],
#         ['total_ili', 'total_patients', 'total_specimins', 
#          'type_a', 'type_b', 'type_unk'],
#         ['total_ili', 'total_patients', 'temperature', 
#          'precipitation'],
#         ['total_ili', 'total_patients', 'total_specimins', 
#          'type_a', 'type_b', 'type_unk','temperature', 
#          'precipitation']
#     ]
# arch_list = ['rnn', 'lstm', 'gru', 'tcn']

def batch_runner(data_dir, feature_list, arch_list, timesteps, batch_size, 
                 epochs, arch, hidden=[], index_col=0, lr=0.001, 
                 val_split=0.2, verbosity=1, shuffle=False, 
                 activation='relu', loss='mean_squared_error',x_label='', 
                 y_label='', title='', save=True, file_name=''):  
    file_list = get_files('data')
    
    for arch in arch_list:
        for file in file_list:
            for features in feature_list:
                print("Creating %s NN" % arch)
                print("using %s as features" %features)
                print("using %s as file" % file)
                
                features_str = '-'.join(features)
                url = './' + data_dir + '/' + file
                filename = arch + '-' + features_str + '-' + file
                if arch.lower() == 'tcn':
                    mse, rmse, r2_value = run.generate_test_model(url, 
                                            features, timesteps, 
                                            batch_size, epochs,
                                            arch, hidden, index_col, lr, 
                                            val_split, verbosity,
                                            shuffle, activation, loss, x_label,
                                            y_label, title, save, 
                                            file_name=filename)
                    
                    # write to csv
                    write_data = [arch, file, features_str, 
                                  mse, rmse, r2_value]
                    write_to_csv('results.csv', write_data)
                else:
                    mse, rmse, r2_value = run.generate_test_model(url, 
                                            features, timesteps, 
                                            batch_size, epochs,
                                            arch, hidden, index_col, lr, 
                                            val_split, verbosity,
                                            shuffle, activation, loss, x_label,
                                            y_label, title, save, 
                                            file_name=filename)

                    # write to csv
                    write_data = [arch, file, features_str, mse, rmse, r2_value]
                    write_to_csv('results.csv', write_data)
