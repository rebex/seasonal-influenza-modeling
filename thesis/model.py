"""
model module works with data to modify models
"""

# imports
from keras.callbacks import EarlyStopping
from keras.models import load_model
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score
from math import sqrt
import layers

""" 
early_stop sets up early stopping which will interupt training when the 
validation loss is no longer improving
https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/EarlyStopping

Inputs:
    monitor (opt): string - metric to monitor
    min_delta (opt): float - value that, if the change in 
            validation loss drops below, will stop training
    patience (opt): int -  Number of epochs with no improvement after 
            which training will be stopped. 
    mode (opt): string - mode determining when to stop training 
            (auto, min, max)

Returns:
    callbacks: list - callbacks to monitor
"""

def early_stop(monitor='val_loss', min_delta=0.001, 
               patience=70, mode='min'):
    earlystop = EarlyStopping(monitor=monitor, min_delta=min_delta,
                              patience=patience, mode=mode, verbose=1)
    callbacks = [earlystop]
    return callbacks

"""
TODO: Python does have some overloading modules - consider using
one of those in lieu of an if statement

change_models allows a user to save, load, or delete a model

Inputs:
    action: string - inform function which action to perform (save, del, 
              load)
    file_name (opt): string - name of model to change.  If load or save 
            model, this field is required.
    model (opt): 

Returns: 
    if action='load' - model
"""

def  change_model(action, file_name='', model=''):  
    if action == 'load' and file_name != '':
        model = load_model(file_name)
        print("The model %s has successfully loaded" % file_name)
        return model
    elif action == 'save' and file_name != '':
        model.save(file_name)
        print("The model %s has successfully saved" % file_name)
    elif action == 'delete' and model != '': 
        del model
    else: print("%s action could not be performed" % action)


"""
prepare_data prepares data to be in the appropriate form to be fed
into the neural network

Inputs:
    test_data: ndarray - data to be tested
    timesteps: int - the number of timesteps to be used

Returns:
    X: ndarray - numpy array with data to be tested
    y: ndarray - numpy array with expected results
"""
def prepare_data(test_data, timesteps):
    X = []
    y = []

    # create array with test data ready to be fed into model
    for i in range(timesteps, test_data.shape[0]):
        X.append(test_data[i-timesteps:i])
        y.append(test_data[i][0])
    X, y = np.array(X), np.array(y)
    
    return X, y

"""
test_model allows model to make predictions using test data

Inputs:
    model: model - model to be tested
    
Return: 
    mse: float - mean squared error value given expected and true values
    rmse: float - root mean squared error given exepected and true values
    r_value: float - r^2 value given expected and true values
    y_test: ndarray - a numpy array of test (expected) values
    y_hat: ndarray - a numpy array of predicted values
"""

def test_model(model, test_data, timesteps): 
    X_test, y_test = prepare_data(test_data, timesteps)
    
    # make predictions using test data
    y_hat = model.predict(X_test)
    mse = mean_squared_error(y_test, y_hat)
    rmse = sqrt(mse)
    r = r2_score(y_test,y_hat)
    
    return y_test, y_hat, mse, rmse, r

"""
fit_model feeds builds and trains models

Inputs:
    train_data: ndarray - data used to train the model
    timesteps: int - the number of timesteps to be used
    epochs: int - epochs to use
    batch_size: int - number of batches
    arch: string - NN architecture to use (rnn, gru, lstm, tcn)
    hidden: int list -  list of integers to be used to determine 
    number of units for each hidden layer within the network
    lr (opt): float - learning rate
    val_split (opt): float - percentage of data to be used as validation
                data (remainder is training)
    verbosity (opt): int - logging verbosity
    shuffle (opt): bool - Shuffle data 
    activation (opt): string - the activation function to use
    loss (opt): string - loss function to use 
Returns:
    history: model - a model object
    loss_history: list - loss function history
    val_history: list - validatio loss history
"""

def fit_model(train_data, timesteps, epochs, batch_size, arch, 
              hidden, lr=0.001, val_split=0.2, verbosity=1, shuffle=True,
              activation='relu', loss='mean_squared_error'):
    # create callback list
    callbacks = early_stop()
    
    # prepare data
    X_train, y_train = prepare_data(train_data, timesteps)
    
    # build model
    if arch.lower() == 'lstm': 
        model = layers.add_lstm(X_train, y_train, hidden, 
                                lr, activation, loss)
    elif arch.lower() == 'gru': 
        model = layers.add_gru(X_train, y_train, hidden, 
                               lr, activation, loss)
    elif arch.lower() == 'rnn':
        model = layers.add_rnn(X_train, y_train, hidden, 
                               lr, activation, loss)
    elif arch.lower() == 'tcn':
        input_shape = train_data.shape[-1]

        model = layers.add_tcn(input_shape, batch_size, timesteps,
                               lr, loss)
    else:
        print('No model built')
        return
    
    # train the data
    history = model.fit(X_train, y_train, epochs=epochs,
                        batch_size=batch_size, 
                        validation_split=val_split,
                        verbose=verbosity, shuffle=shuffle, 
                        callbacks=callbacks)
    model.reset_states()
    
    loss_history = history.history['loss']
    val_history = history.history['val_loss']
    
    return model, loss_history, val_history
    
    